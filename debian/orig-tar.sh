#!/bin/sh -e

# $2 = version, $3 file
TAR=$3
DIR=libdb-je-java-$2.orig

# clean up the upstream tarball
tar -x -z -f $TAR
mv je-$2 $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*/docs/*' $DIR
rm -rf $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
